/**
 * @format
 */

import {AppRegistry, YellowBox} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';
import firebase from '@react-native-firebase/app';

firebase.initializeApp({
    apiKey: "AIzaSyBlhL4BZkoPuxx6T5_uvic4OlopFQ94czM",
    authDomain: "sartm-0001.firebaseapp.com",
    databaseURL: "https://sartm-0001.firebaseio.com",
    projectId: "sartm-0001",
    storageBucket: "sartm-0001.appspot.com",
    messagingSenderId: "286737974087",
    appId: "1:286737974087:web:681f65fa3577db0c3fc262",
    measurementId: "G-QGJRPJF420"
});

//YellowBox.ignoreWarnings(["Require cycle:", "Remote debugger"])

AppRegistry.registerComponent(appName, () => App);
