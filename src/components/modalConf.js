import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Modal} from 'react-native';
class ModalConf extends React.Component {
  render() {
    return (
      <View style={styles.generalcont}>
        <View style={styles.ViewH}>
          <Text style={styles.txt}>
            <Text style={{fontWeight: 'bold'}}>Hola</Text> Andres
          </Text>
        </View>

        <View style={styles.txtMss}>
          <Text style={styles.txt}>
            Quieres avisarle a tu doctor que ya tomaste tu medicamento
          </Text>
        </View>

        <TouchableOpacity style={styles.to}>
          <View style={styles.btnSi}>
            <Text style={styles.textBtn}>SI LO ACABO DE TOMAR</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.to}>
          <View style={styles.btnPos}>
            <Text style={styles.textBtn}>POSPONER 30 MIN</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  btnSi: {
    backgroundColor: '#32CCD6',
    height: 60,
    width: 200,
    borderRadius: 30,
    justifyContent: 'center',
  },
  textBtn: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
    fontFamily: 'WorkSans-Thin',
  },
  btnPos: {
    backgroundColor: '#5C5C5C',
    height: 60,
    width: 200,
    borderRadius: 30,
    justifyContent: 'center',
  },
  txt: {
    marginTop: 10,
    fontFamily: 'WorkSans-Medium',
    fontSize: 22,
  },
  to: {
    marginTop: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  txtMss: {
    width: 300,
  },
  generalcont: {
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 20,
  },
});

export default ModalConf;
