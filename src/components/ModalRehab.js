import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
class ModalRehab extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <View style={styles.generalCont}>
        <View style={styles.grettingCont}>
          <Text style={{fontSize: 22, fontFamily: 'WorkSans-Medium'}}>
            <Text style={{fontWeight: 'bold', fontFamily: 'WorkSans-Medium'}}>
              Hola
            </Text>{' '}
            Andres{' '}
          </Text>
          <Text style={{fontSize: 22, fontFamily: 'WorkSans-Medium'}}>
            Quieres avisarle a tu doctor que vas hacer la actividad{' '}
          </Text>
        </View>
        <View style={styles.viewHoraDescripcion}>
          <View style={styles.horaContainer}>
            <Text style={styles.horaText}>03:00</Text>
            <Text style={styles.horaText}>PM</Text>
          </View>
          <View style={styles.descContainer}>
            <Text style={styles.duracionText}>Duración 30 min. </Text>
            <Text style={styles.ejercicioText}>Ejercicio de manos </Text>
            <Text style={styles.tipoText}>Rehabilitación</Text>
          </View>
        </View>
        <View style={styles.viewIndicaciones}>
          <Text style={{fontFamily: 'WorkSans-Medium'}}>
            Mover el pie de izquierda a derecha en intervalos de 3.
          </Text>
        </View>
        <View style={styles.viewButtons}>
          <TouchableOpacity>
            <View style={styles.btnCancelar}>
              <Text style={styles.btnText}>CANCELAR</Text>
            </View>
          </TouchableOpacity>

          <TouchableOpacity>
            <View style={styles.btnConfrimar}>
              <Text style={styles.btnText}>SI CONFIRMAR</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  generalCont: {
    marginTop: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 'auto',
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 20,
    width: '95%',
  },
  horaContainer: {
    width: 75,
    marginRight: 10,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10,

    elevation: 2,
    borderWidth: 0.001,
    borderRadius: 10,
  },
  horaText: {
    color: '#32CCD6',
    fontSize: 22,
    fontWeight: 'bold',
    fontFamily: 'WorkSans-Thin',
  },
  viewHoraDescripcion: {
    marginTop: 20,
    flex: 0,
    flexDirection: 'row',
    height: 85,
  },
  viewButtons: {
    marginTop: 20,
    flex: 0,
    flexDirection: 'row',
  },
  viewIndicaciones: {
    marginTop: 20,
    alignSelf: 'baseline',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 4.4,
    shadowRadius: 10,

    elevation: 2,
    borderWidth: 0.01,
    borderRadius: 10,
    padding: 10,
  },
  descContainer: {
    width: 230,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10,

    elevation: 2,
    borderWidth: 0.01,
    borderRadius: 10,
  },
  duracionText: {
    fontSize: 17,
    fontFamily: 'WorkSans-Medium',
  },
  ejercicioText: {
    fontSize: 17,
    fontWeight: 'bold',
  },
  tipoText: {
    fontSize: 15,
    fontFamily: 'WorkSans-Medium',
  },
  btnCancelar: {
    backgroundColor: '#5C5C5C',
    width: 150,
    height: 50,
    borderRadius: 30,
    justifyContent: 'center',
    marginRight: 10,
  },
  btnConfrimar: {
    backgroundColor: '#32CCD6',
    width: 150,
    height: 50,
    borderRadius: 30,
    justifyContent: 'center',
  },
  btnText: {
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'WorkSans-Medium',
  },
});
export default ModalRehab;
