import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  TextInput,
  Modal,
} from 'react-native';
import {LocaleConfig, Calendar, CalendarList} from 'react-native-calendars';
import {ModalConf} from '../components/modalConf';
import {ModalRehab} from '../components/ModalRehab';

LocaleConfig.locales.ec = {
  monthNames: [
    'Enero',
    'Febrero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octubre',
    'Noviembre',
    'Diciembre',
  ],
  monthNamesShort: [
    'Ene',
    'Feb',
    'Mar',
    'Jun',
    'Jul',
    'Ago',
    'Sep',
    'Oct',
    'Nov',
    'Dic',
  ],
  dayNames: [
    'Domingo',
    'Lunes',
    'Martes',
    'Miercoles',
    'Jueves',
    'Viernes',
    'Sabado',
  ],
  dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
  today: ['Hoy'],
};
LocaleConfig.defaultLocale = 'ec';
class Treatment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisibleM: false,
      modalVisibleR: false,
    };
  }

  render() {
    const {navigate} = this.props.navigation;
    console.log(this.props.navigation.state.params.data_user.medicament);
    return (
      <View>
        {/*<Text>{}</Text>*/}

        <Modal
          visible={this.state.modalVisibleM}
          transparent={true}
          onRequestClose={() => {
            this.setState({modalVisibleM: false});
          }}
          animationType="slide">
          <ModalConf />
        </Modal>
        <Modal
          visible={this.state.modalVisibleR}
          transparent={true}
          animationType="slide"
          onRequestClose={() => {
            this.setState({modalVisibleR: false});
          }}>
          <ModalRehab />
        </Modal>
        <Calendar
          hideExtraDays={false}
          firstDay={1}
          markedDates={{
            '2020-02-02': {selected: true, selectedColor: 'pink'},
          }}
          style={{height: 130, overflow: 'hidden'}}
          theme={{arrowColor: '#32CCD6'}}
        />
        <TextInput style={{height: 0}} />
        <View>
          <Text style={styles.actividadesDiariasText}>Actividades Diarias</Text>
        </View>

        <FlatList
          contentContainerStyle={{overflow: 'scroll'}}
          style={{height: '100%', overflow: 'scroll'}}
          data={this.props.navigation.state.params.data_user.medicament}
          renderItem={({item}) => (
            <View style={styles.actividadesContainer}>
              <View style={styles.horaContainer}>
                <Text style={styles.horaText}>{item.hora}</Text>
                <Text style={styles.horaText}>PM</Text>
              </View>
              <TouchableOpacity
                onPress={() => {
                  if (item.type == 'M') {
                    this.setState({modalVisibleM: true});
                  } else {
                    this.setState({modalVisibleR: true});
                  }
                }}>
                <View style={styles.descripcionContainer}>
                  <Text style={{fontFamily: 'WorkSans-Medium'}}>
                    {item.dosis}
                  </Text>
                  <Text
                    style={{fontFamily: 'WorkSans-Medium', fontWeight: 'bold'}}>
                    fdsfsfs
                  </Text>
                  <Text style={{fontFamily: 'WorkSans-Medium'}}>
                    Medicamento
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  actividadesDiariasText: {
    fontFamily: 'WorkSans-Medium',
    fontSize: 17,
    marginLeft: 50,
  },
  actividadesContainer: {
    marginTop: 13,
    flex: 1,
    height: 80,
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  horaContainer: {
    backgroundColor: '#fff',
    width: 77,
    justifyContent: 'center',
    borderRadius: 7,
    marginRight: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    height: 70,
    elevation: 5,
  },
  horaText: {
    color: '#32CCD6',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 23,
    fontFamily: 'WorkSans-Medium',
  },
  descripcionContainer: {
    backgroundColor: '#fff',
    width: 167,
    borderRadius: 7,
    height: 70,
    paddingTop: 5,
    paddingLeft: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
export default Treatment;
