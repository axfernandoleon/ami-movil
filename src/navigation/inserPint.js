import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import firebase from '@react-native-firebase/app';
class insertPin extends React.Component {
  static navigationOptions = {
    title: null,
    headerStyle: {height: 0},
  };
  constructor(props) {
    super(props);
    this.state = {
      pin: '',
    };
  }
  render() {
    const {navigate} = this.props.navigation;
    return (
      <ImageBackground
        source={require('../../assets/images/mainBG.png')}
        style={{
          width: '100%',
          height: '100%',
          resize: 'cover',
          position: 'absolute',
        }}>
        <View style={styles.logoContainer}>
          <View style={styles.amiContainer}>
            <Text
              style={{
                fontFamily: 'Baumans-Regular',
                textAlign: 'center',
                fontSize: 96,
                color: '#5C5C5C',
              }}>
              a
            </Text>
            <Text
              style={{
                fontFamily: 'Baumans-Regular',
                textAlign: 'center',
                fontSize: 96,
                color: '#33CDD7',
              }}>
              mi
            </Text>
          </View>

          <Text
            style={{
              fontFamily: 'WorkSans-Medium',
              textAlign: 'center',
              fontSize: 15,
              color: '#5C5C5C',
            }}>
            ACOMPAÑAMIENTO MEDICO REMOTO
          </Text>
        </View>
        <View>
          <TextInput
            style={styles.codeInput}
            placeholder="INGRESE SU CÓDIGO"
            onChangeText={pin => this.setState({pin})}
            value={this.state.pin}
          />
        </View>
        <View style={styles.btnIniciar}>
          <TouchableOpacity
            style={styles.to}
            onPress={() => navigate('Treatments', {pin: this.state.pin})}>
            <Image source={require('../../assets/images/flecha.png')} />
            <Text style={{paddingTop: 10, paddingLeft: 10}}>INICIAR</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  codeInput: {
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#fff',
    width: 272,
    borderRadius: 25,
    margin: 50,
    textAlign: 'center',
    marginTop: 100,
  },
  btnIniciar: {
    width: '40%',
    height: 55,
    backgroundColor: '#fff',
    borderRadius: 30,
    justifyContent: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  to: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
  },
  logoContainer: {
    justifyContent: 'center',
  },
  amiContainer: {
    marginTop: 87,
    marginLeft: 'auto',
    marginRight: 'auto',
    flex: 0,
    flexDirection: 'row',
  },
  BGContainer: {},
});
export default insertPin;
