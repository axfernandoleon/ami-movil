import React from 'react';
import {
  ToolbarAndroid,
  FlatList,
  TextInput,
} from 'react-native-gesture-handler';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Button,
  PushNotificationIOS,
} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import {sendNotificationSchedule,cancelNotifications} from '../utils/notification'

class Treatments extends React.Component {
  data = {};
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      modalMedVisible: false,
      showTimePicker: false,
      date: null,
      pin: this.returnProps() ? this.returnProps().pin : null,
    };
    this.getTreatments(this.state.pin).then(arry => {
      this.setState({data: arry});
    });
    this.data.pin = this.state.pin;
    this.lastId = 0;
  }

  // Get data

  
  async getTreatments(pin) {
    const ftreatments = [];
    const fmedicaments = [];
    const fsintomas = [];
    await firestore()
    .collection('treatment')
    .where('paciente', '==', pin)
    .where('status', '==', true)
    .get()
    .then(activeTreatmentsDataResult => {
      if (activeTreatmentsDataResult.empty) {
        return;
      }
      activeTreatmentsDataResult.forEach(activeTreatmentDoc => {
        ftreatments.push({
          id: activeTreatmentDoc.id,
          info: activeTreatmentDoc.data(),
        });
      });
    })
    .catch(err => {
      console.log('Error getting documents', err);
    });
    
    for (let data of ftreatments) {
      let ref = await firestore()
      .collection(`treatment/${data.id}/medicaments`)
      .get()
      .then(dataResult => {
          if (dataResult.empty) {
            return;
          }
          let array_medicaments = [];
          dataResult.forEach(doc => {
            array_medicaments.push(doc.data());
          });
          fmedicaments.push({
            id: data.id,
            info: data.info,
            medicament: array_medicaments,
          });
        });
      }
      
    for (let data of fmedicaments) {
      let ref = await firestore()
      .collection(`treatment/${data.id}/controlSintomas`)
      .get()
      .then(dataResult => {
          if (dataResult.empty) {
            return;
          }
          let array_sintomas = [];
          dataResult.forEach(doc => {
            array_sintomas.push(doc.data());
          });
          fsintomas.push({
            id: data.id,
            info: data.info,
            medicament: data.medicament,
            control: array_sintomas,
          });
        });
      }
      return fsintomas;
    }

    returnProps() {
      const {params} = this.props.navigation.state;
      return params;
    }
  static navigationOptions = () => {
    return {
      title: 'AMI',
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: '#32CCD6',
      },
    };
  };

  initzialize(data){
    cancelNotifications()
    let controls= data[0].control
    let medicament= data[0].medicament
    controls.forEach((e)=>{sendNotificationSchedule("Notificacion Control")})
    medicament.forEach((e)=>{sendNotificationSchedule("Notificacion Medicament")})

  }
  


  render() {
  const {navigate} = this.props.navigation;
  return (
    <View>
    <TextInput
    style={styles.search}
    placeholder="BUSQUEDA DE TRATAMIENTO"
    />
        <FlatList
          style={{height: '90%'}}
          contentContainerStyle={{overflow: 'hidden'}}
          data={this.state.data}
          renderItem={({item}) => (
            <View style={styles.treatmentContainer}>
              <Image
                source={require('../../assets/images/step.png')}
                style={{marginRight: 10, width: 70, height: 70}}
              />
              <TouchableOpacity
                onPress={() => navigate('Treatment', {data_user: item})}>
                <View style={styles.treatmentDescription}>
                  <View
                    style={{
                      flex: 0,
                      flexDirection: 'column',
                      paddingLeft: 10,
                      paddingTop: 10,
                      width: 150,
                    }}>
                    <Text style={{fontSize: 15, fontFamily: 'WorkSans-Medium'}}>
                      Tratamiento
                    </Text>
                    <Text style={{fontSize: 15, fontFamily: 'WorkSans-Medium'}}>
                      para {item.info.nombreT}
                    </Text>
                    <Text style={{fontSize: 15, fontFamily: 'WorkSans-Medium'}}>
                      Dr. {item.info.doctor}
                    </Text>
                  </View>
                  <View style={{flex: 0, flexDirection: 'column'}}>
                    <Text
                      style={{
                        fontSize: 15,
                        paddingRight: 10,
                        fontFamily: 'WorkSans-Thin',
                      }}>
                      12/12/2019
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
              <Button title={"nope"} onPress={this.initzialize(this.state.data)}>

</Button>
            </View>

          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  treatmentContainer: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 20,
  },
  treatmentDescription: {
    flex: 0,
    width: 230,
    height: 90,
    borderStyle: 'dashed',
    borderWidth: 1,
    borderRadius: 20,
    flexDirection: 'row',
  },
  search: {
    borderWidth: 1,
    width: '90%',
    height: 40,
    marginTop: 20,
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 30,
  },
});
export default Treatments;
