const PushNotification = require("react-native-push-notification");

PushNotification.configure({
    // (optional) Called when Token is generated (iOS and Android)
    onRegister: function (token) {
        console.log("TOKEN:", token);
    },

    // (required) Called when a remote or local notification is opened or received
    onNotification: function (notification) {
        console.log("NOTIFICATION:", notification);

        // process the notification

        // required on iOS only (see fetchCompletionHandler docs: https://github.com/react-native-community/react-native-push-notification-ios)
        notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    // // IOS ONLY (optional): default: all - Permissions to register.
    // permissions: {
    //     alert: true,
    //     badge: true,
    //     sound: true,
    // },

    // Should the initial notification be popped automatically
    // default: true
    popInitialNotification: true,

    /**
     * (optional) default: true
     * - Specified if permissions (ios) and token (android and ios) will requested or not,
     * - if not, you must call PushNotificationsHandler.requestPermissions() later
     * - if you are not using remote notification or do not have Firebase installed, use this:
     *     requestPermissions: Platform.OS === 'ios'
     */
    requestPermissions: true,
});



module.exports = {

    sendNotificationSchedule: (title) => {
        PushNotification.localNotificationSchedule({
            message: `${title}`, // (required)
            date: new Date(Date.now() + 15 * 1000), // in 15 secs
            subText: "Ami Notfication",
            vibration: 600,
            actions: '["Yes", "No"]'
        });
    },

    cancelNotifications:()=>{
        PushNotification.cancelAllLocalNotifications()
    }





}