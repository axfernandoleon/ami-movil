import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import insertPin from './src/navigation/inserPint';
import Treatments from './src/navigation/treatments';
import Treatment from './src/navigation/treatment';
import 'react-native-gesture-handler';
// import modalConf from './src/components/modalConf';
// import {Alert} from 'react-native';
// import firebase from '@react-native-firebase/app';

const AppNavigator = createStackNavigator({
  Home: {screen: insertPin},
  Treatments: {screen: Treatments},
  Treatment: {screen: Treatment},
});

export default createAppContainer(AppNavigator);
